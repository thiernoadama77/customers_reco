# customers_reco

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/thiernoadama77/customers_reco.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/thiernoadama77/customers_reco/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)



# Editing this README



## Name
Gestion des Clients inactifs et fidélisation

## Description
Problème:
Ceci est une application écrit en python avec apache beam. C'est  un ETL qui  nous donne les clients inactifs ( c'est à dire les clients qui n'ont pas passé de commande)
Ceci permetra aux équipes marketing de pourvoir les cibler avec des offres de promo par exemple

Nous avons deux sources de données:
- le fichier "customer2023116.csv" qui contient tous les clients:
  schema: customers_id, customers_fname, customers_lname,...

- Le fihcier "order20231116.csv" qui contient les commandes effectuées
  schema: order_id, order_date, order_customer_id, order_status

- Fichier Output: contient les clients qui n'ont pas effectué de commande
- SChema du output: customer_id, customer_fname, customer_lname

Cette application pourra aussi permettre de charger le resulat dans bigquery. Il suufira de donner en argument le project_id, le dataset et la table

Dans un environement réél il pourra être déployé et schédulé sur Cloud Composer (Airflow).


## License
For open source projects, say how it is licensed.

