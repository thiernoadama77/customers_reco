import argparse
import logging
import os

import apache_beam as beam
from apache_beam.io import ReadFromText
from apache_beam.io import WriteToText
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
from apache_beam.io import WriteToBigQuery
from apache_beam.io.gcp.internal.clients import bigquery

#fonction permettant de filtrer les customers inactif
def is_inactive(custom_order):
    (id_customer, info) = custom_order
    return len(info['orders']) == 0

#function permettant d'avoir un output format csv
def inactive_customer_output(incative_customer):
     (id_customer, info) = incative_customer
     f_name, l_name = info['customers_info'][0]
     return f"{id_customer},{f_name},{l_name}"


# fonction principale
def run(argv=None, save_main_session=True):
    """Main entry point; defines and runs the inactive customers pipeline."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input_customers',
        dest='input_customers',
        default=os.path.join('.','customer20231116.csv'),
        help='Customer Input file to process.')
    parser.add_argument(
        '--input_orders',
        dest='input_orders',
        default=os.path.join('.','order20231116.csv'),
        help='Orders Input file to process.')
    parser.add_argument(
        '--output',
        dest='output',
        default='inactive_customers',
        help='Output file to write results to.')
    parser.add_argument(
        '--project_id',
        dest='project_id',
        required=False,
        help='project gcp  ')
    
    parser.add_argument(
        '--bq_dataset',
        dest='bq_dataset',
        required=False,
        help=' bigquery dataset  ')
    parser.add_argument(
        '--bq_table',
        dest='bq_table',
        required=False,
        help=' bigquery bq_table  ')

    known_args, pipeline_args = parser.parse_known_args(argv)

    with beam.Pipeline() as p:
            #chargement  du fichier customers et split pour avoir un format (k, (v,w)) avec k = customer_id, (v=first_name, w=last_name)
            customer_map = (
                p | 'Read_customers' >> ReadFromText(known_args.input_customers)
                | beam.Map(lambda line: (line.split(',')[0], (line.split(',')[1], line.split(',')[2])))           
            )
            # Chargement du fichier order et split pour avoir un format (k, v) avec k =customer_id 
            order_map = (
                p | 'Read_orders' >> ReadFromText(known_args.input_orders)
                | beam.Map(lambda line: (line.split(',')[2],1))
            )
            # Faire une jointure avec la fonction beam CoGroupByKey()
            customer_joining_order = (({'customers_info': customer_map, 'orders': order_map}) 
                                      |  'Merge' >> beam.CoGroupByKey() )
            # Filter les customers inactifs
            inactive_customers = customer_joining_order | beam.Filter(is_inactive) \
                                 | beam.Map(inactive_customer_output) 
            # Enregister dans un fichier output
            inactive_customers   | 'Write' >> WriteToText(known_args.output)

            # Si on a un projet GCP avec les droits en écriture dans une dable bigquery: cette étape fait le chargement bigquery
            if known_args.project_id is not None and  known_args.bq_dataset is not None :
                table_spec = bigquery.TableReference(
                projectId=known_args.project_id,
                datasetId= known_args.bq_dataset,
                tableId= known_args.bq_table)
                table_schema = {
                    'fields': [{
                        'name': 'customer_id', 'type': 'STRING', 'mode': 'NULLABLE'
                    }, {
                        'name': 'first_name', 'type': 'STRING', 'mode': 'NULLABLE'
                    },
                    {
                        'name': 'last_name', 'type': 'STRING', 'mode': 'NULLABLE'
                    }
                    ]
                }
                inactive_customers | beam.io.WriteToBigQuery(
                                        table_spec,
                                        schema=table_schema,
                                        write_disposition=beam.io.BigQueryDisposition.WRITE_TRUNCATE,
                                        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    run()