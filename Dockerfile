FROM python:3.9-slim-buster

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY inactive_customers_bnp.py .
COPY *.csv .

CMD ["python", "inactive_customers_bnp.py"]
